var footballService = require('sports');

function init() {
	footballService.getTeamInfo(function(teamData) {
		if (teamData) {
			$.testLabel.text = teamData.resultsCount;
			$.anotherTestLabel.text = teamData.headlines[0].headline;
			var tableData = [];

			var block = Titanium.UI.createView({
				top : 0,
				backgroundColor : '#AB172D',
				height : 70
			});

			//$.index.add(block);

			for (var i = 0; i < teamData.resultsLimit; i++) {
				var row = Ti.UI.createTableViewRow({
					rowIndex : i, // custom property, useful for determining the row during events
					height : 70,
					backgroundColor : '#FFFFFB'
				});

				var image = teamData.headlines[i].images[0];
				var imageURL;
				if (image == "" || image == null) {
					imageURL = "http://www.imms.org/images/dolphin_presentation/dolphin1.jpg";
				} else {
					imageURL = teamData.headlines[i].images[0].url;
				}
				// Ti.API.info(teamData.headlines[i].images[0].url);

				var imageAvatar = Ti.UI.createImageView({
					image : imageURL,
					left : 5,
					right : 5,
					top : 5,
					width : 80,
					height : 50
				});
				row.add(imageAvatar);
				
				articleLink = teamData.headlines[i].links.mobile.href;
				Ti.API.info(articleLink);
				
				var eachHeadline = Ti.UI.createLabel({
					color : '#474448',
					font : {
						fontFamily : 'Arial',
						fontSize : 15,
						fontWeight : 'bold'
					},
					text : teamData.headlines[i].headline,
					left : 100,
					top : 4,
					bottom : 4,
					height : 50,
					id : "headline" + i,
					headlineLink: articleLink
				});
				
				eachHeadline.addEventListener('click', function(e) {
				//	var intent = Ti.Android.createIntent({
				//		action : Ti.Android.ACTION_VIEW,
				//		data : articleLink
				//	});
				//	Ti.Android.currentActivity.startActivity(intent);
				
					Titanium.Platform.openURL(e.source.headlineLink);
				});

				row.add(eachHeadline);

				var timeStamp = Ti.UI.createLabel({
					color : 'grey',
					font : {
						fontFamily : 'Arial',
						fontSize : 8,
						fontWeight : 'normal'
					},
					text : teamData.headlines[i].lastModified.substring(0, 10),
					left : 20,
					top : 60
				});
				row.add(timeStamp);

				tableData.push(row);
			}
			
			var webRow = Ti.UI.createTableViewRow({
				rowIndex : i, // custom property, useful for determining the row during events
				height : 250,
				backgroundColor : '#FFFFFB'
			});
			
			var webview = Ti.UI.createWebView({
				url: 'https://jenniferlong.wufoo.com/forms/z1cbnurp18vzz4i/',
				//url: 'https://www.surveymonkey.com/s/KDXWRMY',
				top: -40,
				showScrollbar: true,
				backgroundColor : '#70DBDB'
			});
				

			webRow.add(webview);
			
			tableData.push(webRow);
			
			var tableView = Ti.UI.createTableView({
				backgroundColor : '#a6a6a6',
				data : tableData,
				top : 73
			});

			$.index.add(tableView);
		}

		//Adds event listener that closes webview and corresponding row after the user navigates to another page (in our case, hits "submit")
		webview.addEventListener('load', function(e){
			//if (webview.url != 'https://www.surveymonkey.com/s/KDXWRMY'){
			if (webview.url != 'https://jenniferlong.wufoo.com/forms/z1cbnurp18vzz4i/'){
				webview.hide();
				webRow.setHeight(0);
			}
			else{
				Ti.API.info("Loading original page");
			}
		});
		
		
		var fb = require('facebook');
		fb.appid = '403308983140162';
		fb.permissions = ['publish_actions']; // Permissions your app needs
		fb.forceDialogAuth = false;
		fb.addEventListener('login', function(e) {
		    if (e.success) {
		        alert('Logged In');
		    } 
		    else if (e.error) {
		        alert(e.error);
		    } 
		    else if (e.cancelled) {
		        alert("Canceled");
		    }
		});
		fb.logout();
		//fb.authorize();	
		$.index.add(fb.createLoginButton({
    		top : 50,
   		 	style : fb.BUTTON_STYLE_WIDE
		}));
	
		
		/*
		fb.requestWithGraphPath('me/feed', {message: "Trying out FB Graph API and it's fun!"}, "POST", function(e) {
	   		if (e.success) {
	       		 alert("Success!  From FB: " + e.result);
	    	} 
	    	else {
	        	if (e.error) {
	            	alert(e.error);
	       		} 
	       		else {
	            	alert("Unkown result");
	        	}
    		}
    	});
    	*/
    	
    	/*
    	$.index.fbAsyncInit = function() {
	        FB.init({
	          appId      : '403308983140162',
	          xfbml      : true,
	          version    : 'v2.0'
	        });
      	};

      (function(d, s, id){
         var js, fjs = d.getElementsByTagName(s)[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement(s); js.id = id;
         js.src = "//connect.facebook.net/en_US/sdk.js";
         fjs.parentNode.insertBefore(js, fjs);
       }(document, 'script', 'facebook-jssdk'));
		  
    	FB.ui({
		  method: 'share',
		  href: 'https://developers.facebook.com/docs/',
		}, function(response){});
		*/
    	
    });
}

	
	
/*
var social = require('alloy/social').create({
    consumerSecret: 'VDbkqWgqdS7ccqB6j9yCbCD0QtW7BSaMiKuaWFtZsFhXW2sI6u',
    consumerKey: 'MlHGQgsNfMadFDfpcjrXkYrP9'
});

  // If not authorized, get authorization from the user
if(!social.isAuthorized()) {
    social.authorize();
    Ti.API.info("Just called authorize");
}

// Post a message
 // Setup both callbacks for confirmation
 // Note: share() automatically calls authorize() so an explicit call as above is unnecessary
social.share({
    message: "Salut, Monde!",
    success: function(e) {alert('Success!');},
    error: function(e) {alert('Error!');}
});

// Deauthorize the application
social.deauthorize();
*/


function refresh() {
	init();
}

$.index.open();
init();
