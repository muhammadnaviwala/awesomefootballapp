function Controller() {
    function init() {
        footballService.getTeamInfo(function(teamData) {
            if (teamData) {
                $.testLabel.text = teamData.resultsCount;
                $.anotherTestLabel.text = teamData.headlines[0].headline;
                var tableData = [];
                Titanium.UI.createView({
                    top: 0,
                    backgroundColor: "#AB172D",
                    height: 70
                });
                for (var i = 0; teamData.resultsLimit > i; i++) {
                    var row = Ti.UI.createTableViewRow({
                        rowIndex: i,
                        height: 70,
                        backgroundColor: "#FFFFFB"
                    });
                    var image = teamData.headlines[i].images[0];
                    var imageURL;
                    imageURL = "" == image || null == image ? "http://www.imms.org/images/dolphin_presentation/dolphin1.jpg" : teamData.headlines[i].images[0].url;
                    var imageAvatar = Ti.UI.createImageView({
                        image: imageURL,
                        left: 5,
                        right: 5,
                        top: 5,
                        width: 80,
                        height: 50
                    });
                    row.add(imageAvatar);
                    articleLink = teamData.headlines[i].links.mobile.href;
                    Ti.API.info(articleLink);
                    var eachHeadline = Ti.UI.createLabel({
                        color: "#474448",
                        font: {
                            fontFamily: "Arial",
                            fontSize: 15,
                            fontWeight: "bold"
                        },
                        text: teamData.headlines[i].headline,
                        left: 100,
                        top: 4,
                        bottom: 4,
                        height: 50,
                        id: "headline" + i,
                        headlineLink: articleLink
                    });
                    eachHeadline.addEventListener("click", function(e) {
                        Titanium.Platform.openURL(e.source.headlineLink);
                    });
                    row.add(eachHeadline);
                    var timeStamp = Ti.UI.createLabel({
                        color: "grey",
                        font: {
                            fontFamily: "Arial",
                            fontSize: 8,
                            fontWeight: "normal"
                        },
                        text: teamData.headlines[i].lastModified.substring(0, 10),
                        left: 20,
                        top: 60
                    });
                    row.add(timeStamp);
                    tableData.push(row);
                }
                var webRow = Ti.UI.createTableViewRow({
                    rowIndex: i,
                    height: 250,
                    backgroundColor: "#FFFFFB"
                });
                var webview = Ti.UI.createWebView({
                    url: "https://jenniferlong.wufoo.com/forms/z1cbnurp18vzz4i/",
                    top: -40,
                    showScrollbar: true,
                    backgroundColor: "#70DBDB"
                });
                webRow.add(webview);
                tableData.push(webRow);
                var tableView = Ti.UI.createTableView({
                    backgroundColor: "#a6a6a6",
                    data: tableData,
                    top: 73
                });
                $.index.add(tableView);
            }
            webview.addEventListener("load", function() {
                if ("https://jenniferlong.wufoo.com/forms/z1cbnurp18vzz4i/" != webview.url) {
                    webview.hide();
                    webRow.setHeight(0);
                } else Ti.API.info("Loading original page");
            });
        });
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    this.__controllerPath = "index";
    arguments[0] ? arguments[0]["__parentSymbol"] : null;
    arguments[0] ? arguments[0]["$model"] : null;
    arguments[0] ? arguments[0]["__itemTemplate"] : null;
    var $ = this;
    var exports = {};
    $.__views.index = Ti.UI.createWindow({
        backgroundColor: "white",
        id: "index"
    });
    $.__views.index && $.addTopLevelView($.__views.index);
    $.__views.Heading = Ti.UI.createLabel({
        width: "100%",
        height: 70,
        color: "#2C3A5D",
        top: 0,
        backgroundColor: "#AB172D",
        textAlign: "center",
        font: {
            fontFamily: "Arial",
            fontSize: 40,
            fontWeight: "bold"
        },
        text: "Houston Texans",
        id: "Heading"
    });
    $.__views.index.add($.__views.Heading);
    $.__views.testLabel = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        id: "testLabel"
    });
    $.__views.index.add($.__views.testLabel);
    $.__views.anotherTestLabel = Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        id: "anotherTestLabel"
    });
    $.__views.index.add($.__views.anotherTestLabel);
    exports.destroy = function() {};
    _.extend($, $.__views);
    var footballService = require("sports");
    var social = require("alloy/social").create({
        consumerSecret: "VDbkqWgqdS7ccqB6j9yCbCD0QtW7BSaMiKuaWFtZsFhXW2sI6u",
        consumerKey: "MlHGQgsNfMadFDfpcjrXkYrP9"
    });
    social.isAuthorized() || social.authorize();
    social.share({
        message: "Salut, Monde!",
        success: function() {
            alert("Success!");
        },
        error: function() {
            alert("Error!");
        }
    });
    social.deauthorize();
    $.index.open();
    init();
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._;

module.exports = Controller;